import time
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

klucz = b'jakisklucz123456'

def Encrypt2MB_ECB(klucz):
    ecb = AES.new(klucz, AES.MODE_ECB)
    with open('2MB.txt', 'rb') as plik:
        zawartosc_pliku = plik.read()
    plik.close()
    start = time.time()
    zaszyfrowany_tekst = ecb.encrypt(pad(zawartosc_pliku, AES.block_size))
    stop = time.time()
    print("Czas szyfrowania pliku przy użyciu ECB: " + str(stop - start))
    with open ('2MB_zaszyfrowany_ECB.txt', 'wb') as zaszyfrowany_plik: #zapisanie zaszyfrowanej zawartosci pliku
        zaszyfrowany_plik.write(zaszyfrowany_tekst)
    zaszyfrowany_plik.close()


def Encrypt2MB_CBC(klucz):
    cbc = AES.new(klucz, AES.MODE_CBC)
    with open('2MB.txt', 'rb') as plik:
        zawartosc_pliku = plik.read()
    plik.close()
    start = time.time()
    zaszyfrowany_tekst = cbc.encrypt(pad(zawartosc_pliku, AES.block_size))
    stop = time.time()
    print("Czas szyfrowania pliku przy użyciu CBC: " + str(stop - start))
    with open('2MB_zaszyfrowany_CBC.txt', 'wb') as zaszyfrowany_plik:  # zapisanie zaszyfrowanej zawartosci pliku
        zaszyfrowany_plik.write(cbc.iv)
        zaszyfrowany_plik.write(zaszyfrowany_tekst)
    zaszyfrowany_plik.close()


def Encrypt2MB_OFB(klucz):
    ofb = AES.new(klucz, AES.MODE_OFB)
    with open('2MB.txt', 'rb') as plik:
        zawartosc_pliku = plik.read()
    plik.close()
    start = time.time()
    zaszyfrowany_tekst = ofb.encrypt(zawartosc_pliku)  # szyfrowanie zawartosci pliku
    stop = time.time()
    print("Czas szyfrowania pliku przy użyciu OFB: " + str(stop - start))
    with open('2MB_zaszyfrowany_OFB.txt', 'wb') as zaszyfrowany_plik:  # zapisanie zaszyfrowanej zawartosci pliku
        zaszyfrowany_plik.write(zaszyfrowany_tekst)
    zaszyfrowany_plik.close()


def Encrypt2MB_CFB(klucz):
    cfb = AES.new(klucz, AES.MODE_CFB)
    with open('2MB.txt', 'rb') as plik:
        zawartosc_pliku = plik.read()
    plik.close()
    start = time.time()
    zaszyfrowany_tekst = cfb.encrypt(zawartosc_pliku)  # szyfrowanie zawartosci pliku
    stop = time.time()
    print("Czas szyfrowania pliku przy użyciu CFB: " + str(stop - start))
    with open('2MB_zaszyfrowany_CFB.txt', 'wb') as zaszyfrowany_plik:  # zapisanie zaszyfrowanej zawartosci pliku
        zaszyfrowany_plik.write(zaszyfrowany_tekst)
    zaszyfrowany_plik.close()


def Encrypt2MB_CTR(klucz):
    ctr = AES.new(klucz, AES.MODE_CTR)
    with open('2MB.txt', 'rb') as plik:
        zawartosc_pliku = plik.read()
    plik.close()
    start = time.time()
    zaszyfrowany_tekst = ctr.encrypt(zawartosc_pliku)  # szyfrowanie zawartosci pliku
    stop = time.time()
    print("Czas szyfrowania pliku przy użyciu CTR: " + str(stop - start))
    with open('2MB_zaszyfrowany_CTR.txt', 'wb') as zaszyfrowany_plik:  # zapisanie zaszyfrowanej zawartosci pliku
        zaszyfrowany_plik.write(zaszyfrowany_tekst)
    zaszyfrowany_plik.close()

def Decrypt2MB_ECB(klucz):
    zaszyfrowany_plik = open ('2MB_zaszyfrowany_ECB.txt', 'rb')
    zaszyfrowany_tekst = zaszyfrowany_plik.read()
    iv = zaszyfrowany_plik.read(16)
    zaszyfrowany_plik.close()
    start = time.time()
    ecb = AES.new(klucz, AES.MODE_ECB, iv=iv)
    odszyfrowany_tekst = unpad(ecb.decrypt(zaszyfrowany_tekst), AES.block_size)
    stop = time.time()
    print("Czas deszyfrowania pliku przy użyciu ECB: " + str(stop - start))
    with open ('2MB_odszyfrowany_ECB.txt', 'wb') as odszyfrowany_plik:
        odszyfrowany_plik.write(odszyfrowany_tekst)
    odszyfrowany_plik.close()

def Decrypt2MB_CBC(klucz):
    zaszyfrowany_plik = open('2MB_zaszyfrowany_CBC.txt', 'rb')
    iv = zaszyfrowany_plik.read(16)
    zaszyfrowany_tekst = zaszyfrowany_plik.read()
    zaszyfrowany_plik.close()
    start = time.time()
    cbc = AES.new(klucz, AES.MODE_CBC, iv=iv)
    odszyfrowany_tekst = unpad(cbc.decrypt(zaszyfrowany_tekst), AES.block_size)
    stop = time.time()
    print("Czas deszyfrowania pliku przy użyciu ECB: " + str(stop - start))
    with open('2MB_odszyfrowany_CBC.txt', 'wb') as odszyfrowany_plik:
        odszyfrowany_plik.write(odszyfrowany_tekst)
    odszyfrowany_plik.close()


def Decrypt2MB_OFB(klucz):
    ofb = AES.new(klucz, AES.MODE_OFB)
    with open('2MB_zaszyfrowany_OFB.txt', 'rb') as zaszyfrowany_plik:
        zaszyfrowany_tekst = zaszyfrowany_plik.read()
    zaszyfrowany_plik.close()
    start = time.time()
    odszyfrowany_tekst = ofb.decrypt(zaszyfrowany_tekst)
    stop = time.time()
    print("Czas deszyfrowania pliku przy użyciu OFB: " + str(stop - start))
    with open('2MB_odszyfrowany_OFB.txt', 'wb') as odszyfrowany_plik:
        odszyfrowany_plik.write(odszyfrowany_tekst)
    odszyfrowany_plik.close()


def Decrypt2MB_CFB(klucz):
    cfb = AES.new(klucz, AES.MODE_CFB)
    with open('2MB_zaszyfrowany_CFB.txt', 'rb') as zaszyfrowany_plik:
        zaszyfrowany_tekst = zaszyfrowany_plik.read()
    zaszyfrowany_plik.close()
    start = time.time()
    odszyfrowany_tekst = cfb.decrypt(zaszyfrowany_tekst)
    stop = time.time()
    print("Czas deszyfrowania pliku przy użyciu CFB: " + str(stop - start))
    with open('2MB_odszyfrowany_CFB.txt', 'wb') as odszyfrowany_plik:
        odszyfrowany_plik.write(odszyfrowany_tekst)
    odszyfrowany_plik.close()


def Decrypt2MB_CTR(klucz):
    ctr = AES.new(klucz, AES.MODE_CTR)
    with open('2MB_zaszyfrowany_CTR.txt', 'rb') as zaszyfrowany_plik:
        zaszyfrowany_tekst = zaszyfrowany_plik.read()
    zaszyfrowany_plik.close()
    start = time.time()
    odszyfrowany_tekst = ctr.decrypt(zaszyfrowany_tekst)
    stop = time.time()
    print("Czas deszyfrowania pliku przy użyciu CTR: " + str(stop - start))
    with open('2MB_odszyfrowany_CTR.txt', 'wb') as odszyfrowany_plik:
        odszyfrowany_plik.write(odszyfrowany_tekst)
    odszyfrowany_plik.close()


Encrypt2MB_ECB(klucz)